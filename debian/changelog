python-couchdb (0.6-2) UNRELEASED; urgency=low

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed homepage (https)
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient XS-Python-Version field
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Jakub Wilk <jwilk@debian.org>  Sun, 05 May 2013 16:02:16 +0200

python-couchdb (0.6-1) unstable; urgency=low

  [ Noah Slater ]
  * Updated debian/control, updated Build-Depends on debhelper to 7.2.11.

  [ Piotr Ożarowski ]
  * New upstream release. Closes: #537456
  * Add python-cjson as an alternative dependency to python-simplejson and
    python >= 2.6 (due to new couchdb/json.py)
  * python2.5_compatibility.patch added
  * Remove PYTHONPATH from debian/rules, not really needed
  * Standards-Version bumped to 3.8.3 (no other changes needed)

 -- Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>  Tue, 18 Aug 2009 19:42:22 +0200

python-couchdb (0.5-1) experimental; urgency=low

  * New upstream release.
  * Added debian/README.source file.
  * Updated debian/control, updated Vcs-Browser.
  * Updated debian/control, updated Depends.
  * Updated debian/control, updated Description.
  * Updated debian/copyright, added additional copyright holder.
  * Updated debian/rules, improved uscan options for get-orig-source.

 -- Noah Slater <nslater@tumbolia.org>  Sun, 18 Jan 2009 20:26:37 +0000

python-couchdb (0.4-1) unstable; urgency=low

  * New upstream release.
  * Updated debian/control, added Build-Depends.
  * Updated debian/control, updated Standards-Version to 3.8.0.
  * Updated debian/copyright, updated for latest format proposal.
  * Updated debian/rules, added API documentation generation.

 -- Noah Slater <nslater@tumbolia.org>  Thu, 10 Jul 2008 15:47:33 +0100

python-couchdb (0.3-2) unstable; urgency=low

  * Updated debian/control, updated Suggests. Closes: #469027

 -- Noah Slater <nslater@tumbolia.org>  Tue, 04 Mar 2008 23:20:56 +0000

python-couchdb (0.3-1) unstable; urgency=low

  * Initial release. Closes: #447596

 -- Noah Slater <nslater@tumbolia.org>  Tue, 12 Feb 2008 21:57:28 +0000
