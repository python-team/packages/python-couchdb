Source: python-couchdb
Section: python
Priority: optional
Maintainer: David Paleino <dapal@debian.org>
Build-Depends:
 debhelper-compat (= 9)
 , python
Build-Depends-Indep:
 help2man
 , python-sphinx
 , python-pygments
 , python-simplejson
 , python-setuptools (>= 0.6b3)
 , dh-python
Standards-Version: 3.9.6
Homepage: https://pypi.python.org/pypi/CouchDB
Vcs-Git: https://salsa.debian.org/python-team/packages/python-couchdb.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-couchdb

Package: python-couchdb
Architecture: all
Depends:
 ${python:Depends}
 , ${misc:Depends}
 , python-simplejson | python (>= 2.6) | python-cjson
 , libjs-jquery
 , libjs-underscore
Suggests: couchdb
Description: library for working with Apache CouchDB
 Provides a high-level client library for Apache CouchDB, a view server and dump
 and load utilities that can be used as migration tools when upgrading or moving
 between databases.
 .
 Apache CouchDB is a distributed document database system with bi-directional
 replication. It makes it simple to build collaborative applications that can
 be replicated offline by users, with full interactivity (query, add, update,
 delete), and later "synced up" with everyone else's changes when back online.
